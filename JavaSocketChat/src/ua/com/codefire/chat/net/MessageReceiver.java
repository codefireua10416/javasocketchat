/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.chat.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class MessageReceiver implements Runnable {

    private static final String RESPONSE_UNKNOWN_COMMAND = "UNKNOWN COMMAND";

    private final ServerSocket serverSocket;
    private State currentState = State.STOPPED;
    private int port;

    private final List<MessageReceiverListener> listeners;

    /**
     *
     * @param port
     * @throws IOException
     */
    public MessageReceiver(int port) throws IOException {
        this.port = port;
        this.serverSocket = new ServerSocket(port);
        this.serverSocket.setSoTimeout(1000);
        this.listeners = Collections.synchronizedList(new ArrayList<MessageReceiverListener>());
    }

    /**
     *
     * @param currentState
     */
    protected void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    /**
     *
     * @param listener
     * @return
     */
    public boolean addListener(MessageReceiverListener listener) {
        return listeners.add(listener);
    }

    /**
     *
     * @param listener
     * @return
     */
    public boolean removeListener(MessageReceiverListener listener) {
        return listeners.remove(listener);
    }

    /**
     *
     * @return
     */
    public synchronized boolean listen() {
        switch (currentState) {
            case STOPPED:
                new Thread(this).start();
                return true;
        }

        return false;
    }

    /**
     *
     * @return
     */
    public synchronized boolean stop() {
        switch (currentState) {
            case LISTENING:
                setCurrentState(State.STOPPING);
                return true;
        }

        return false;
    }

    @Override
    public void run() {
        setCurrentState(State.LISTENING);

        while (currentState == State.LISTENING) {
            try (Socket income = serverSocket.accept()) {
                DataInputStream dis = new DataInputStream(income.getInputStream());
                DataOutputStream dos = new DataOutputStream(income.getOutputStream());

                try {
                    Command command = Command.valueOf(dis.readUTF());

                    switch (command) {
                        case MESSAGE:
                            // 1. Address; 2. Message
//                            String address = dis.readUTF();
                            String address = income.getInetAddress().getHostAddress();
                            String nick = dis.readUTF();
                            String message = dis.readUTF();
                            
                            dos.writeUTF(Response.SUCCESS.toString());
                            dos.flush();
                            
                            Thread.sleep(500);

                            for (MessageReceiverListener listener : listeners) {
                                listener.messageReceived(address, nick, message);
                            }
                            break;
                    }
                } catch (IllegalArgumentException ex) {
                    dos.writeUTF(RESPONSE_UNKNOWN_COMMAND);
                    dos.flush();
                } catch (InterruptedException ex) {
                    Logger.getLogger(MessageReceiver.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (SocketTimeoutException ex) {
                ;
            } catch (IOException ex) {
                Logger.getLogger(MessageReceiver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        setCurrentState(State.STOPPED);
    }

    /**
     * 
     */
    private enum State {
        STOPPED, LISTENING, STOPPING;
    }
}
