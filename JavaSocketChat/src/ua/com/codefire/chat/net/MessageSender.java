/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.chat.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class MessageSender {

    private int serverPort;
    private String nick;

    public MessageSender(int serverPort) {
        this.serverPort = serverPort;
    }

    public MessageSender(int serverPort, String nick) {
        this.serverPort = serverPort;
        this.nick = nick;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     *
     * @param address
     * @param content
     * @return
     */
    public boolean sendMessage(String address, String content) {

        try (Socket client = new Socket(address, serverPort)) {
            DataOutputStream dos = new DataOutputStream(client.getOutputStream());
            DataInputStream dis = new DataInputStream(client.getInputStream());

            dos.writeUTF(Command.MESSAGE.toString());
            dos.writeUTF(nick == null ? "Unknown" : nick);
            dos.writeUTF(content);
            dos.flush();

            switch (Response.valueOf(dis.readUTF())) {
                case SUCCESS:
                    return true;
            }
        } catch (IOException ex) {
            Logger.getLogger(MessageSender.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

}
